provider "google" {

}

data "terraform_remote_state" "project" {
	backend = "gcs" 

	config = {
    bucket = "st-terraform-test"
    prefix = "testing/terraform.tfstate"
	}
}

data "terraform_remote_state" "red" {
	backend = "gcs" 

	config = {
    bucket = "st-terraform-test"
    prefix = "subnet/terraform.tfstate"
	}
}

resource "google_compute_instance" "first-instance" {
    name = var.name
    machine_type = var.machine_type
    project = data.terraform_remote_state.project.outputs.project
    zone = var.zone[0]

    labels = var.labels

    boot_disk {
        initialize_params {
            image = var.image
            size  = var.size
        }
    }

    network_interface {
        network = data.terraform_remote_state.project.outputs.network
        subnetwork = data.terraform_remote_state.red.outputs.subnet-gce
    }
}