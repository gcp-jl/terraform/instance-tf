output "instancia" {
    value = google_compute_instance.first-instance.instance_id
}

output "instance-ip" {
    value = google_compute_instance.first-instance.network_interface.0.network_ip
}