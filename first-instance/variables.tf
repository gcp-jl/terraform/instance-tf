
variable "name" {
    type = string
    default = "test"
}

variable "machine_type" {
    type = string
    default = "g1-small"
}

variable "zone" {
    type = list
    default = ["us-central1-a"]
}

variable "labels" {
    type = map
    default = {
        owner = "jeyseven"
		tier  = "test"
		object = "instance"
    }
}
    
variable "image" {
    type = string
    default = "centos-7"
}

variable "size" {
    type = string
    default = "30"
}
